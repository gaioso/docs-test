================
Titulo documento
================

-------------------
Subtitulo documento
-------------------

Introdución
===========

.. _inicio:

This document is meant to give a tutorial-like overview of all common tasks
while using Sphinx.

Instalar Sphinx
---------------

Install Sphinx, either from a distribution package or from
`PyPI <https://pypi.python.org/pypi/Sphinx>`_ with ::

   $ pip install Sphinx

Imos ver o que pasa con outro parágrafo.

what
  Definition lists associate a term with a definition.

::

  Una parte de código para ejecutar en bash
  pero no se como puede aparecer en HTML.

.. image:: images/biohazard.png
   :alt: Texto para mostrar
   :align: right


Algunhas opcións:

-a		Lee todos os ficheiros.
-f file		Elimina o ficheiro.
--long		Versión longa

Imos volver ao inicio_.
