.. Administración electrónica documentation master file, created by
   sphinx-quickstart on Fri Aug 11 06:27:54 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Benvido/a á Administración electrónica's documentation!
=======================================================

Contidos:

.. toctree::
   :maxdepth: 1

   introducion
   tutorial
   configuracion

.. # * :ref:`genindex`
.. # * :ref:`modindex`
.. # * :ref:`search`

